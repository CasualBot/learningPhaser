/**
 * Created by shawn on 15-06-22.
 */

var loadState = {

	preload: function() {
		// Creating a loading label
		var loadingLabel = game.add.text(game.world.centerX, 150, "Loading...", {
			font: '30px',
			fill: '#ffffff'
		});

		loadingLabel.anchor.setTo(0.5,0.5);

		//Creating the loading bar
		var progressBar = game.add.sprite(game.world.centerX, 200, 'progressBar');
		progressBar.anchor.setTo(0.5,0.5);

		game.load.setPreloadSprite(progressBar);

		//Loading assets
		game.load.image('player', 'assets/player.png');
		game.load.image('enemy', 'assets/enemy.png');
		game.load.image('coin', 'assets/coin.png');
		game.load.image('wallV', 'assets/wallVertical.png');
		game.load.image('wallH', 'assets/wallHorizontal.png');
		//new background
		game.load.image('background', 'assets/background.png');
	},

	create: function() {
		game.state.start('menu');
	}

};
